# == Schema Information
#
# Table name: page_link_types
#
#  id           :integer          not null, primary key
#  page_id      :integer
#  link_type_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  parent_id    :integer
#

class PageLinkTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_pages_link_type, only: [:show, :edit, :update, :destroy]

  # GET /pages_link_types
  # GET /pages_link_types.json
  def index
    @pages_link_types = PageLinkType.all
  end

  # GET /pages_link_types/1
  # GET /pages_link_types/1.json
  def show
  end

  # GET /pages_link_types/new
  def new
    @pages_link_type = PageLinkType.new
  end

  # GET /pages_link_types/1/edit
  def edit
  end

  # POST /pages_link_types
  # POST /pages_link_types.json
  def create
    @pages_link_type = PageLinkType.new(pages_link_type_params)

    respond_to do |format|
      if @pages_link_type.save
        format.html { redirect_to @pages_link_type, notice: 'Pages link type was successfully created.' }
        format.json { render :show, status: :created, location: @page_link_type }
      else
        format.html { render :new }
        format.json { render json: @pages_link_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages_link_types/1
  # PATCH/PUT /pages_link_types/1.json
  def update
    respond_to do |format|
      if @pages_link_type.update(pages_link_type_params)
        format.html { redirect_to @pages_link_type, notice: 'Pages link type was successfully updated.' }
        format.json { render :show, status: :ok, location: @pages_link_type }
      else
        format.html { render :edit }
        format.json { render json: @pages_link_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages_link_types/1
  # DELETE /pages_link_types/1.json
  def destroy
    @link_type = @pages_link_type.link_type
    @pages_link_type.destroy
    respond_to do |format|
      format.html { redirect_to link_type_url(@link_type), notice: 'Pages link type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pages_link_type
      @pages_link_type = PageLinkType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pages_link_type_params
      params.require(:pages_link_type).permit(:page_id, :link_type_id, :parent_id)
    end
end
