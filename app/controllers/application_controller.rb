class ApplicationController < ActionController::Base
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
    protect_from_forgery with: :exception

	rescue_from ActiveRecord::RecordNotFound do
	  flash[:notice] = 'The page you tried to access does not exist'
	  redirect_to root_path
	end

	rescue_from ActionController::RoutingError, :with => :error_render_method

	def error_render_method
	  flash[:notice] = 'The page you tried to access does not exist'
	  redirect_to root_path
	end

    def after_sign_in_path_for(resource)
      dashboards_path
    end

    def after_inactive_sign_up_path_for(resource)
  	  trading_path
    end

    def after_sign_up_path_for(resource)
	  trading_path
	end

end
