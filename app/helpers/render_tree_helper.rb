module RenderTreeHelper
  class Render
    class << self
      attr_accessor :h, :options

      def render_node(h, options)
        @h, @options = h, options

        node = options[:node]
        "
          <li>
              #{ show_link }
            #{ children }
          </li>
        "
      end

      def show_link
        node = options[:node]
        ns   = options[:namespace]
        url = h.url_for(:controller => 'static_page'.pluralize, :action => :show, :id => node.page.slug)
        title_field = options[:title]
        "#{ h.link_to(node.page.title, url,:class => 'nodrop-down') }"
      end

      def children
        unless options[:children].blank?
          "<ul>#{ options[:children] }</ul>"
        end
      end

    end
  end
end