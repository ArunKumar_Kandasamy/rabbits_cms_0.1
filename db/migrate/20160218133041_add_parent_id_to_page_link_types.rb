class AddParentIdToPageLinkTypes < ActiveRecord::Migration
  def change
    add_column :page_link_types, :parent_id, :integer
  end
end
