class CreateGenerals < ActiveRecord::Migration
  def change
    create_table :generals do |t|
      t.attachment :logo
      t.attachment :favicon
      t.string :page_title
      t.text :meta_description
      t.text :analytics_text

      t.timestamps null: false
    end
  end
end
