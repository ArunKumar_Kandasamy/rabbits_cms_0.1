# == Schema Information
#
# Table name: gallery_types
#
#  id         :integer          not null, primary key
#  name       :string
#  height     :integer
#  width      :integer
#  is_video   :boolean          default(FALSE)
#  image_type :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class GalleryTypesController < ApplicationController
      before_action :authenticate_user!
      before_action :set_gallery_type, only: [:show, :edit, :update, :destroy]

      # GET /gallery_types
      # GET /gallery_types.json

      def index
        @gallery_types = GalleryType.all
      end

      # GET /gallery_types/1
      # GET /gallery_types/1.json
      def show
      end

      # GET /gallery_types/new
      def new
        @gallery_type = GalleryType.new
      end

      # GET /gallery_types/1/edit
      def edit
      end

      # POST /gallery_types
      # POST /gallery_types.json
      def create
        @gallery_type = GalleryType.create(gallery_type_params)

        respond_to do |format|
          if @gallery_type.save
            format.html { redirect_to @gallery_type, notice: 'Gallery type was successfully created.' }
            format.json { render :show, status: :created, location: @gallery_type }
          else
            format.html { render :new }
            format.json { render json: @gallery_type.errors, status: :unprocessable_entity }
          end
        end
      end

      # PATCH/PUT /gallery_types/1
      # PATCH/PUT /gallery_types/1.json
      def update
           @gallery_type.name = params[:gallery_type][:name]
           @gallery_type.height = params[:gallery_type][:height]
           @gallery_type.width = params[:gallery_type][:width]
           @gallery_type.is_video = params[:gallery_type][:is_video]
           @gallery_type.image_type = params[:gallery_type][:image_type]
          if @gallery_type.save
                 flash[:notice] =  'Gallery type was successfully updated.' 
                 redirect_to @gallery_type
          else
                 render :edit
          end
      end

      # DELETE /gallery_types/1
      # DELETE /gallery_types/1.json
      def destroy
        @gallery_type.destroy
        respond_to do |format|
          format.html { redirect_to gallery_types_url, error: 'Gallery type was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private

          # Use callbacks to share common setup or constraints between actions.
          def set_gallery_type
            @gallery_type = GalleryType.find(params[:id])
          end

          # Never trust parameters from the scary internet, only allow the white list through.
          def gallery_type_params
            params.require(:gallery_type).permit(:name, :height, :width, :is_video, :image_type)
          end
end
