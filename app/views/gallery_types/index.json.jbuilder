json.array!(@gallery_types) do |gallery_type|
  json.extract! gallery_type, :id, :name, :height, :width, :is_video, :image_type, :slug
  json.url gallery_type_url(gallery_type, format: :json)
end
