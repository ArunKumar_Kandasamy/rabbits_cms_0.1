json.array!(@contents) do |content|
  json.extract! content, :id, :content_block_id, :title, :description, :sort_order
  json.url content_url(content, format: :json)
end
