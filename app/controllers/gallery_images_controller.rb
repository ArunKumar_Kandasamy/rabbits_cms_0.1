# == Schema Information
#
# Table name: gallery_images
#
#  id                  :integer          not null, primary key
#  avatar_file_name    :string
#  avatar_content_type :string
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  gallery_type_id     :integer
#  alt_text            :string
#  sort_order          :integer
#  link                :string
#  description         :text
#  title               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class GalleryImagesController < ApplicationController
    before_action :authenticate_user!
    before_action :set_gallery_type
    before_action :set_gallery_image, only: [:show, :edit, :update, :destroy]

    # GET /gallery_images
    # GET /gallery_images.json
    def index
      @gallery_images = @gallery_type.gallery_images
    end

    # GET /gallery_images/1
    # GET /gallery_images/1.json
    def show
    end

    # GET /gallery_images/new
    def new
      @gallery_image = GalleryImage.new
    end

    # GET /gallery_images/1/edit
    def edit
    end

    # POST /gallery_images
    # POST /gallery_images.json
    def create
          uploaded_io = params[:gallery_image][:avatar]
          File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename.gsub(" ","_")).to_s.gsub("innbox.in","innboxapi.in").gsub("innbox_client","innbox_api"), 'wb') do |file|
                file.write(uploaded_io.read)
          end
          @gallery_image = GalleryImage.create(gallery_image_params)
          respond_to do |format|
              if @gallery_image.save
                format.html { redirect_to gallery_type_path(@gallery_type), notice: 'Gallery image was successfully created.' }
                format.json { render :show, status: :created, location: @gallery_image }
              else
                format.html { render :new }
                format.json { render json: @gallery_image.errors, status: :unprocessable_entity }
              end
           end
    end

    # PATCH/PUT /gallery_images/1
    # PATCH/PUT /gallery_images/1.json
    def update
        if params[:gallery_image][:avatar].blank?
              if params[:gallery_image][:crop_x].present?
                    @gallery_image.crop_x = params[:gallery_image][:crop_x]
                    @gallery_image.crop_y = params[:gallery_image][:crop_y]
                    @gallery_image.crop_w = params[:gallery_image][:crop_w]
                    @gallery_image.crop_h = params[:gallery_image][:crop_h]
                    @gallery_image.save
                    flash[:notice] = "Gallery image was successfully updated."
                    redirect_to gallery_type_url(@gallery_type)
              else
                    @gallery_image.title = params[:gallery_image][:title]
                    @gallery_image.alt_text = params[:gallery_image][:alt_text]
                    @gallery_image.link = params[:gallery_image][:link]
                    @gallery_image.description = params[:gallery_image][:description]
                    @gallery_image.tag_list = params[:gallery_image][:tag_list]
                    if @gallery_image.save
                           render :action => "crop"
                    else
                           render :edit 
                    end
              end
        else
              uploaded_io = params[:gallery_image][:avatar]
              File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename.gsub(" ","_")).to_s.gsub("innbox_client","innbox_api"), 'wb') do |file|
                    file.write(uploaded_io.read)
              end
              @gallery_image.title = params[:gallery_image][:title]
              @gallery_image.alt_text = params[:gallery_image][:alt_text]
              @gallery_image.link = params[:gallery_image][:link]
              @gallery_image.description = params[:gallery_image][:description]
              @gallery_image.avatar = params[:gallery_image][:avatar]
              if @gallery_image.save
                     render :action => "crop"
              else
                     render :edit 
              end
        end
    end

    def sort
      params[:gallery_image].each_with_index do |id, index|
        @gallery_image = @gallery_type.gallery_images.find(id)
        @gallery_image.sort_order =  index+1
        @gallery_image.save
      end
      render nothing: true
    end

    # DELETE /gallery_images/1
    # DELETE /gallery_images/1.json
    def destroy
          @gallery_image.destroy
          respond_to do |format|
            format.html { 
              redirect_to gallery_type_gallery_images_url(@gallery_type)
              flash[:error] = " Gallery image was successfully destroyed. " 
             }
            format.json { head :no_content }
          end
    end

    private

          # Use callbacks to share common setup or constraints between actions.
          def set_gallery_type
            @gallery_type = GalleryType.find(params[:gallery_type_id])
          end

          # Use callbacks to share common setup or constraints between actions.
          def set_gallery_image
            @gallery_image = GalleryImage.find(params[:id])
          end

          # Never trust parameters from the scary internet, only allow the white list through.
          def gallery_image_params
              params.require(:gallery_image).permit(:avatar, :gallery_type_id, :alt_text, :sort_order, :link, :description, :title, :crop_x, :crop_y, :crop_w, :crop_h, :tag_list)
          end
end
