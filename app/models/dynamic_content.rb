# == Schema Information
#
# Table name: dynamic_contents
#
#  id          :integer          not null, primary key
#  title       :string
#  slug        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  description :text
#

class DynamicContent < ActiveRecord::Base
	extend FriendlyId
    friendly_id :title, use: :slugged
    validates :title, :presence => true, :uniqueness => true
    validates :description, :presence => true


    def should_generate_new_friendly_id?
      title_changed?
    end
end
