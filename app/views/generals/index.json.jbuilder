json.array!(@generals) do |general|
  json.extract! general, :id, :logo, :favicon, :page_title, :meta_description, :analytics_text
  json.url general_url(general, format: :json)
end
