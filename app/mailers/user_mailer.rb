class UserMailer < ActionMailer::Base

  default from: "preethi.m@coderabbits.com"

  def message_for_transportation(transport_name, transport_company_name, transport_email, transport_phone, transport_pickup_from, transport_drop_to,transport_required_vehicle, transport_enquiry,transport_man_power_count)
    @transport_name = transport_name
    @transport_company_name = transport_company_name
    @transport_email = transport_email
    @transport_phone = transport_phone
    @transport_pickup_from = transport_pickup_from
    @transport_drop_to = transport_drop_to
    @transport_man_power_count = @transport_man_power_count
    @transport_required_vehicle = transport_required_vehicle
    @transport_enquiry = transport_enquiry
    mail(
      :subject => "Transportation Enquiry from #{transport_name}- #{transport_email}",
      :to  => "preethi.m@coderabbits.com")
  end

  def message_for_movers(trading_name, trading_company_name, trading_email, trading_phone, trading_pickup_from, trading_drop_to, trading_enquiry, trading_man_power_count)
    @trading_name = trading_name
    @trading_company_name = trading_company_name
    @trading_email = trading_email
    @trading_phone = trading_phone
    @trading_pickup_from = trading_pickup_from
    @trading_drop_to = trading_drop_to
    @trading_man_power_count = trading_man_power_count
    @trading_enquiry = trading_enquiry
    mail(
      :subject => "Movers Enquiry from #{trading_name}- #{trading_email}",
      :to  => "preethi.m@coderabbits.com")
  end

  def message_for_secured_storage(storage_name, storage_company_name, storage_email, storage_phone, storage_things, storage_enquiry)
    @storage_name = storage_name
    @storage_company_name = storage_company_name
    @storage_email = storage_email
    @storage_phone = storage_phone
    @storage_things = storage_things
    @storage_enquiry = storage_enquiry
    mail(
      :subject => "Secured Storage Enquiry from #{storage_name}- #{storage_email}",
      :to  => "preethi.m@coderabbits.com")
  end

  # you can then put any of your own methods here
end

