# == Schema Information
#
# Table name: gallery_images
#
#  id                  :integer          not null, primary key
#  avatar_file_name    :string
#  avatar_content_type :string
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  gallery_type_id     :integer
#  alt_text            :string
#  sort_order          :integer
#  link                :string
#  description         :text
#  title               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

module GalleryImagesHelper
end
