# == Schema Information
#
# Table name: link_types
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class LinkTypesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_link_type, only: [:show, :edit, :update, :destroy, :assign]

  # GET /link_types
  # GET /link_types.json
  def index
    @link_types = LinkType.order(:name)
    respond_to do |format|
      format.html
      format.json { render json: @link_types.tokens(params[:q]) }
    end
  end

  # GET /link_types/1
  # GET /link_types/1.json
  def show
    @filter_pages = Page.all.map{|x| x.id}
    @filter_link_types = @link_type.page_link_types.map{|x| x.page_id}
    @remaining_pages = @filter_pages - @filter_link_types
    @pages = Page.find(@remaining_pages)
  end

  # GET /link_types/new
  def new
    @link_type = LinkType.new
  end

  # GET /link_types/1/edit
  def edit
  end


  def rebuild
    @page_link_type = PageLinkType.find(params[:id])
    @page_link_type.update(:parent_id => params[:parent_id])
    render text: ""
  end

  def assign
    params[:page_ids].each do |id|
      PageLinkType.create(page_id: id,link_type_id: @link_type.id)
    end
    redirect_to link_type_url(@link_type)
  end

  # POST /link_types
  # POST /link_types.json
  def create
    @link_type = LinkType.new(link_type_params)
    respond_to do |format|
      if @link_type.save
        format.html { redirect_to @link_type, notice: 'Link type was successfully created.' }
        format.json { render :show, status: :created, location: @link_type }
      else
        format.html { render :new }
        format.json { render json: @link_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /link_types/1
  # PATCH/PUT /link_types/1.json
  def update
    respond_to do |format|
      if @link_type.update(link_type_params)
        format.html { redirect_to @link_type, notice: 'Link type was successfully updated.' }
        format.json { render :show, status: :ok, location: @link_type }
      else
        format.html { render :edit }
        format.json { render json: @link_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /link_types/1
  # DELETE /link_types/1.json
  def destroy
    @link_type.destroy
    respond_to do |format|
      format.html { redirect_to link_types_url, notice: 'Link type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_link_type
      @link_type = LinkType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def link_type_params
      params.require(:link_type).permit(:name)
    end
end
