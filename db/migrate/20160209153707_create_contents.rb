class CreateContents < ActiveRecord::Migration
  def change
    create_table :contents do |t|
      t.integer :content_block_id
      t.string :title
      t.string :description
      t.integer :sort_order, :default => 0

      t.timestamps null: false
    end
  end
end
