class AddPhoneNumberToGenerals < ActiveRecord::Migration
  def change
    add_column :generals, :phone_number, :string
    add_column :generals, :fb_link, :string
    add_column :generals, :twitter_link, :string
    add_column :generals, :address, :text
    add_column :generals, :email_id, :string
  end
end
