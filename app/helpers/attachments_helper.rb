# == Schema Information
#
# Table name: attachments
#
#  id                 :integer          not null, primary key
#  name               :string
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  image_updated_at   :datetime
#  docs_file_name     :string
#  docs_content_type  :string
#  docs_file_size     :integer
#  docs_updated_at    :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

module AttachmentsHelper
end
