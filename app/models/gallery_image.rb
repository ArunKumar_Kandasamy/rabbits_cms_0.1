# == Schema Information
#
# Table name: gallery_images
#
#  id                  :integer          not null, primary key
#  avatar_file_name    :string
#  avatar_content_type :string
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  gallery_type_id     :integer
#  alt_text            :string
#  sort_order          :integer
#  link                :string
#  description         :text
#  title               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class GalleryImage < ActiveRecord::Base
  #GEMS USED
  #ACCESSORS
  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h, :processing, :deferred_image, :tag_list
  attr_writer :deferred_image
  #ASSOCIATIONS
  has_many :taggings, :dependent => :destroy, class_name: "Tagging"
  has_many :tags, through: :taggings
  has_attached_file :avatar, :url  => "/uploads/:id/:style_:basename.:extension", styles: lambda { |a| a.instance.is_image? ? {:large => a.instance.styles,:medium => "300x300!", :thumb => "100x100!"}  : {:thumb => { :geometry => "100x100#", :format => 'jpg', :time => 10}, :medium => { :geometry => "300x300#", :format => 'jpg', :time => 10}}},
processors: lambda { |a| a.is_video? ? "" : [ :cropper ] }
  belongs_to :gallery_type, class_name:"GalleryType"
  #VALIDATIONS
  validates :title, :presence => true
  validates_attachment_content_type :avatar, :content_type => ['application/x-shockwave-flash', 'application/x-shockwave-flash', 'application/flv', 'video/mp4','image/jpeg', 'image/jpg', 'image/png']
  #CALLBACKS
  after_update :reprocess_avatar, :if => :cropping?
  after_create :update_sort_order
  after_save :assign_deferred_image
  before_save :link_validation
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def decode_image_data

      if self.image_data.present?
          # If image_data is present, it means that we were sent an image over
          # JSON and it needs to be decoded.  After decoding, the image is processed
          # normally via Paperclip.
          if self.image_data.present?
              data = StringIO.new(Base64.decode64(self.image_data))
              data.class.class_eval {attr_accessor :original_filename, :content_type}
              data.original_filename = self.id.to_s + ".png"
              data.content_type = "image/png"

              self.avatar = data
          end
      end
  end

  def update_sort_order
     @sort_maximum= GalleryImage.maximum("sort_order") ? GalleryImage.maximum("sort_order") : 0
      @sort_maximum+=1
      self.sort_order=@sort_maximum
      self.save
  end

  def validate_image_size
    if avatar.file? && avatar.size > get_current_file_size_limit
      errors.add_to_base(" ... Your error message")
    end
  end 

  def styles
    if self.gallery_type.present?
      return "#{self.gallery_type.width}x#{self.gallery_type.height}!"
    else                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
      return "1000x1000!"
    end
  end

  def is_image?
    [ 'image/jpeg', 
      'image/jpg',
      'image/png' ].include?(avatar.content_type)
  end

  def is_video?
    [ 'application/x-mp4',
      'video/mp4',
      'video/mpeg',
      'video/quicktime',
      'video/x-la-asf',
      'video/x-ms-asf',
      'video/x-msvideo',
      'video/x-sgi-movie',
      'video/x-flv',
      'flv-application/octet-stream',
      'video/3gpp',
      'video/3gpp2',
      'video/3gpp-tt',
      'video/BMPEG',
      'video/BT656',
      'video/CelB',
      'video/DV',
      'video/H261',
      'video/H263',
      'video/H263-1998',
      'video/H263-2000',
      'video/H264',
      'video/JPEG',
      'video/MJ2',
      'video/MP1S',
      'video/MP2P',
      'video/MP2T',
      'video/mp4',
      'video/MP4V-ES',
      'video/MPV',
      'video/mpeg4',
      'video/mpeg4-generic',
      'video/nv',
      'video/parityfec',
      'video/pointer',
      'video/raw',
      'video/rtx' ].include?(avatar.content_type)
  end

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end


  def avatar_geometry(style = :original)
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file(avatar.path(style))
  end

  #JOBS
  #PRIVATE 

  def reprocess_avatar
    return unless (cropping? && !processing)
    self.processing = true
    avatar.reprocess!
    self.processing = false
  end

  def assign_deferred_image
    if @deferred_image
      self.avatar = @deferred_image
      @deferred_image = nil
      save!
    end
  end
  
  def link_validation
    if self.link.match("https://").present?
    elsif self.link.match("http://").present?
    else
      self.link = "http://" + self.link
    end
  end


  def self.tagged_with(name)
      Tag.find_by_name!(name).gallery_images
  end

  def self.tag_counts
      Tag.select("tags.*, count(taggings.tag_id) as count").joins(:taggings).group("taggings.tag_id")
  end
  
  def tag_list
    tags.map(&:name).join(", ")
  end
  
  def tag_list=(names)
    self.tags = names.split(",").map do |n|
      Tag.where(name: n.strip).first_or_create!
    end
  end
end
