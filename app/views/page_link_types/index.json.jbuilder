json.array!(@pages_link_types) do |pages_link_type|
  json.extract! pages_link_type, :id, :page_id, :link_type
  json.url pages_link_type_url(pages_link_type, format: :json)
end
