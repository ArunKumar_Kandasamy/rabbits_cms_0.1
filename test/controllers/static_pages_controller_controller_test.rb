require 'test_helper'

class StaticPagesControllerControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get 404" do
    get :404
    assert_response :success
  end

  test "should get about_us" do
    get :about_us
    assert_response :success
  end

  test "should get board_member" do
    get :board_member
    assert_response :success
  end

  test "should get contact_us" do
    get :contact_us
    assert_response :success
  end

  test "should get donate" do
    get :donate
    assert_response :success
  end

  test "should get events" do
    get :events
    assert_response :success
  end

  test "should get help_and_hands" do
    get :help_and_hands
    assert_response :success
  end

  test "should get join_volunteers" do
    get :join_volunteers
    assert_response :success
  end

  test "should get our_team" do
    get :our_team
    assert_response :success
  end

  test "should get projects" do
    get :projects
    assert_response :success
  end

end
