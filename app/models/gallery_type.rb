# == Schema Information
#
# Table name: gallery_types
#
#  id         :integer          not null, primary key
#  name       :string
#  height     :integer
#  width      :integer
#  is_video   :boolean          default(FALSE)
#  image_type :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class GalleryType < ActiveRecord::Base
	has_many :gallery_images, class_name:"GalleryImage"
    #VALIDATIONS
    validates :name, :presence => true, :uniqueness => true
    validates :image_type, :presence => true, :uniqueness => true
    validates :width, :presence => true
    validates :height, :presence => true
    #CALLBACKS
    before_save :downcase_fields

    #SCOPES
    #CUSTOM SCOPES
    #OTHER METHODS
    #JOBS
    #PRIVATE
      
    def downcase_fields
      self.name.downcase!
      self.image_type.downcase!
    end
end
