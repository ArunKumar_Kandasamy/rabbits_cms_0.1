# == Schema Information
#
# Table name: gallery_types
#
#  id         :integer          not null, primary key
#  name       :string
#  height     :integer
#  width      :integer
#  is_video   :boolean          default(FALSE)
#  image_type :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module GalleryTypesHelper
end
