# == Schema Information
#
# Table name: page_link_types
#
#  id           :integer          not null, primary key
#  page_id      :integer
#  link_type_id :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  parent_id    :integer
#

class PageLinkType < ActiveRecord::Base
	belongs_to :page, class_name: "Page"
	belongs_to :link_type, class_name: "LinkType"
end
