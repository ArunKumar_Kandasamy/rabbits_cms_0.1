class RenameTablePagesLinkTypes < ActiveRecord::Migration
  def change
  	 rename_table :pages_link_types, :page_link_types
  end
end
