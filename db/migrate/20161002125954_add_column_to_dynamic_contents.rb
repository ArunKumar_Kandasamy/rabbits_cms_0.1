class AddColumnToDynamicContents < ActiveRecord::Migration
  def change
    add_column :dynamic_contents, :brief, :text
    add_column :dynamic_contents, :opt1, :string
    add_column :dynamic_contents, :opt2, :string
    add_column :dynamic_contents, :opt3, :string
    add_column :dynamic_contents, :opt4, :string
    add_column :dynamic_contents, :opt5, :string
  end
end
