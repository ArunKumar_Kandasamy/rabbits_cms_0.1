# == Schema Information
#
# Table name: content_blocks
#
#  id         :integer          not null, primary key
#  name       :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ContentBlock < ActiveRecord::Base
	extend FriendlyId
    friendly_id :name, use: :slugged
	has_many :contents
    validates :name, presence: true

    def should_generate_new_friendly_id?
      name_changed?
    end
end
