# == Schema Information
#
# Table name: link_types
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class LinkType < ActiveRecord::Base

  has_many :page_link_types, class_name: "PageLinkType"
  has_many :pages, through: :page_link_types, class_name: "Page"
  validates :name, presence: true

	def self.tokens(query)
	  link_types = where("name like ?", "%#{query}%")
	  if link_types.empty?
	    [{id: "<<<#{query}>>>", name: "New: \"#{query}\""}]
	  else
	    link_types
	  end
	end
  
  def self.ids_from_tokens(tokens)
    tokens.gsub!(/<<<(.+?)>>>/) { create!(name: $1).id }
    tokens
  end

end
