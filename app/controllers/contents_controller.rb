# == Schema Information
#
# Table name: contents
#
#  id               :integer          not null, primary key
#  content_block_id :integer
#  title            :string
#  sort_order       :integer          default(0)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  description      :text
#  brief            :text
#  opt1             :string
#  opt2             :string
#  opt3             :string
#  opt4             :string
#  opt5             :string
#

class ContentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_content_block
  before_action :set_content, only: [:show, :edit, :update, :destroy]

  # GET /contents
  # GET /contents.json
  def index
    @contents = Content.all
  end

  # GET /contents/1
  # GET /contents/1.json
  def show
  end

  # GET /contents/new
  def new
    @content = Content.new
  end

  # GET /contents/1/edit
  def edit
  end

  # POST /contents
  # POST /contents.json
  def create
    @content = Content.new(content_params)
    respond_to do |format|
      if @content.save
        format.html { redirect_to content_block_path(@content_block), notice: 'Content was successfully created.' }
        format.json { render :show, status: :created, location: @content }
      else
        format.html { render :new }
        format.json { render json: @content.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contents/1
  # PATCH/PUT /contents/1.json
  def update
    respond_to do |format|
      if @content.update(content_params)
        format.html { redirect_to content_block_path(@content_block), notice: 'Content was successfully updated.' }
        format.json { render :show, status: :ok, location: @content }
      else
        format.html { render :edit }
        format.json { render json: @content.errors, status: :unprocessable_entity }
      end
    end
  end


  def sort
    params[:content].each_with_index do |id, index|
      Content.where(id: id).update_all(sort_order: index+1)
    end
    render nothing: true
  end

  # DELETE /contents/1
  # DELETE /contents/1.json
  def destroy
    @content.destroy
    respond_to do |format|
      format.html { redirect_to content_block_path(@content_block), notice: 'Content was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_content_block
      @content_block = ContentBlock.find_by_slug(params[:content_block_id])
    end

    def set_content
      @content = Content.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def content_params
      params.require(:content).permit(:content_block_id, :title, :description, :sort_order, :brief, :opt1, :opt2, :opt3, :opt4, :opt5)
    end
end
