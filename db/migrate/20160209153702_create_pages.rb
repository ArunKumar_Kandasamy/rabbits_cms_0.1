class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.string :description
      t.integer :parent_id
      t.string :meta_title
      t.string :meta_description
      t.string :slug
      t.boolean :status
      t.integer :sort_order, :default => 0

      t.timestamps null: false
    end
  end
end
