# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#

class UsersController < ApplicationController
      before_action :authenticate_user!, only: [ :index, :destroy] 
      before_action :set_user, only: [ :show, :destroy]

      # GET /users
      # GET /users.json
      def index
        @users = User.all
      end

      def show
      end

      # GET /users/new
      def new
        @user = User.new
      end

      # GET /users/1/edit
      def edit
          @user = User.find(params[:id])
      end

      # POST /users
      # POST /users.json
      def create
          @user = User.new(user_params)
          respond_to do |format|
            if @user.save
              format.html { redirect_to login_path, notice: 'Registration successfull!' }
            else
              format.html { render :new }
            end
          end
      end

      # PATCH/PUT /users/1
      # PATCH/PUT /users/1.json
      def update
        @user = User.find(params[:id])
          if @user.update(user_params)
            session[:user_id] = @user.id
            redirect_to user_path(@user), notice: "Signed in!"
          else
             render :edit 
          end
      end

      # DELETE /users/1
      # DELETE /users/1.json
      def destroy
            @user = User.find(params[:id])
            @user.destroy
            respond_to do |format|
              format.html { redirect_to users_path, notice: 'User was successfully destroyed.' }
              format.json { head :no_content }
            end
      end

      private
          # Use callbacks to share common setup or constraints between actions.
          def set_user
              if current_user.present?
                  @user = current_user
              else
                  redirect_to login_url
              end
          end

          # Never trust parameters from the scary internet, only allow the white list through.
          def user_params
            params.require(:user).permit()
          end
end
