class AddColumnsToContents < ActiveRecord::Migration
  def change
    add_column :contents, :brief, :text
    add_column :contents, :opt1, :string
    add_column :contents, :opt2, :string
    add_column :contents, :opt3, :string
    add_column :contents, :opt4, :string
    add_column :contents, :opt5, :string
  end
end
