json.array!(@attachments) do |attachment|
  json.extract! attachment, :id, :name, :image, :docs
  json.url attachment_url(attachment, format: :json)
end
