--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: attachments; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE attachments (
    id integer NOT NULL,
    name character varying,
    image_file_name character varying,
    image_content_type character varying,
    image_file_size integer,
    image_updated_at timestamp without time zone,
    docs_file_name character varying,
    docs_content_type character varying,
    docs_file_size integer,
    docs_updated_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE attachments OWNER TO innovfox;

--
-- Name: attachments_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE attachments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE attachments_id_seq OWNER TO innovfox;

--
-- Name: attachments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE attachments_id_seq OWNED BY attachments.id;


--
-- Name: ckeditor_assets; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE ckeditor_assets (
    id integer NOT NULL,
    data_file_name character varying NOT NULL,
    data_content_type character varying,
    data_file_size integer,
    assetable_id integer,
    assetable_type character varying(30),
    type character varying(30),
    width integer,
    height integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE ckeditor_assets OWNER TO innovfox;

--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE ckeditor_assets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ckeditor_assets_id_seq OWNER TO innovfox;

--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE ckeditor_assets_id_seq OWNED BY ckeditor_assets.id;


--
-- Name: content_blocks; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE content_blocks (
    id integer NOT NULL,
    name character varying,
    slug character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE content_blocks OWNER TO innovfox;

--
-- Name: content_blocks_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE content_blocks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE content_blocks_id_seq OWNER TO innovfox;

--
-- Name: content_blocks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE content_blocks_id_seq OWNED BY content_blocks.id;


--
-- Name: contents; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE contents (
    id integer NOT NULL,
    content_block_id integer,
    title character varying,
    sort_order integer DEFAULT 0,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    description text,
    brief text,
    opt1 character varying,
    opt2 character varying,
    opt3 character varying,
    opt4 character varying,
    opt5 character varying
);


ALTER TABLE contents OWNER TO innovfox;

--
-- Name: contents_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE contents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contents_id_seq OWNER TO innovfox;

--
-- Name: contents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE contents_id_seq OWNED BY contents.id;


--
-- Name: dynamic_contents; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE dynamic_contents (
    id integer NOT NULL,
    title character varying,
    slug character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    description text
);


ALTER TABLE dynamic_contents OWNER TO innovfox;

--
-- Name: dynamic_contents_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE dynamic_contents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dynamic_contents_id_seq OWNER TO innovfox;

--
-- Name: dynamic_contents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE dynamic_contents_id_seq OWNED BY dynamic_contents.id;


--
-- Name: friendly_id_slugs; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE friendly_id_slugs (
    id integer NOT NULL,
    slug character varying NOT NULL,
    sluggable_id integer NOT NULL,
    sluggable_type character varying(50),
    scope character varying,
    created_at timestamp without time zone
);


ALTER TABLE friendly_id_slugs OWNER TO innovfox;

--
-- Name: friendly_id_slugs_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE friendly_id_slugs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE friendly_id_slugs_id_seq OWNER TO innovfox;

--
-- Name: friendly_id_slugs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE friendly_id_slugs_id_seq OWNED BY friendly_id_slugs.id;


--
-- Name: gallery_images; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE gallery_images (
    id integer NOT NULL,
    avatar_file_name character varying,
    avatar_content_type character varying,
    avatar_file_size integer,
    avatar_updated_at timestamp without time zone,
    gallery_type_id integer,
    alt_text character varying,
    sort_order integer,
    link character varying,
    description text,
    title character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE gallery_images OWNER TO innovfox;

--
-- Name: gallery_images_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE gallery_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gallery_images_id_seq OWNER TO innovfox;

--
-- Name: gallery_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE gallery_images_id_seq OWNED BY gallery_images.id;


--
-- Name: gallery_types; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE gallery_types (
    id integer NOT NULL,
    name character varying,
    height integer,
    width integer,
    is_video boolean DEFAULT false,
    image_type character varying,
    slug character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE gallery_types OWNER TO innovfox;

--
-- Name: gallery_types_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE gallery_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gallery_types_id_seq OWNER TO innovfox;

--
-- Name: gallery_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE gallery_types_id_seq OWNED BY gallery_types.id;


--
-- Name: generals; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE generals (
    id integer NOT NULL,
    logo_file_name character varying,
    logo_content_type character varying,
    logo_file_size integer,
    logo_updated_at timestamp without time zone,
    favicon_file_name character varying,
    favicon_content_type character varying,
    favicon_file_size integer,
    favicon_updated_at timestamp without time zone,
    page_title character varying,
    meta_description text,
    analytics_text text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    phone_number character varying,
    fb_link character varying,
    twitter_link character varying,
    address text,
    email_id character varying
);


ALTER TABLE generals OWNER TO innovfox;

--
-- Name: generals_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE generals_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE generals_id_seq OWNER TO innovfox;

--
-- Name: generals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE generals_id_seq OWNED BY generals.id;


--
-- Name: link_types; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE link_types (
    id integer NOT NULL,
    name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE link_types OWNER TO innovfox;

--
-- Name: link_types_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE link_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE link_types_id_seq OWNER TO innovfox;

--
-- Name: link_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE link_types_id_seq OWNED BY link_types.id;


--
-- Name: mercury_images; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE mercury_images (
    id integer NOT NULL,
    image_file_name character varying,
    image_content_type character varying,
    image_file_size integer,
    image_updated_at timestamp without time zone,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE mercury_images OWNER TO innovfox;

--
-- Name: mercury_images_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE mercury_images_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE mercury_images_id_seq OWNER TO innovfox;

--
-- Name: mercury_images_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE mercury_images_id_seq OWNED BY mercury_images.id;


--
-- Name: page_link_types; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE page_link_types (
    id integer NOT NULL,
    page_id integer,
    link_type_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    parent_id integer
);


ALTER TABLE page_link_types OWNER TO innovfox;

--
-- Name: page_link_types_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE page_link_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE page_link_types_id_seq OWNER TO innovfox;

--
-- Name: page_link_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE page_link_types_id_seq OWNED BY page_link_types.id;


--
-- Name: pages; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE pages (
    id integer NOT NULL,
    title character varying,
    parent_id integer,
    meta_title character varying,
    meta_description character varying,
    slug character varying,
    status boolean,
    sort_order integer DEFAULT 0,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    description text
);


ALTER TABLE pages OWNER TO innovfox;

--
-- Name: pages_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE pages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE pages_id_seq OWNER TO innovfox;

--
-- Name: pages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE pages_id_seq OWNED BY pages.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE schema_migrations OWNER TO innovfox;

--
-- Name: taggings; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE taggings (
    id integer NOT NULL,
    tag_id integer,
    gallery_image_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE taggings OWNER TO innovfox;

--
-- Name: taggings_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE taggings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE taggings_id_seq OWNER TO innovfox;

--
-- Name: taggings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE taggings_id_seq OWNED BY taggings.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE tags (
    id integer NOT NULL,
    name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE tags OWNER TO innovfox;

--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tags_id_seq OWNER TO innovfox;

--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE tags_id_seq OWNED BY tags.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: innovfox
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying DEFAULT ''::character varying NOT NULL,
    reset_password_token character varying,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0 NOT NULL,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip inet,
    last_sign_in_ip inet,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    confirmation_token character varying,
    confirmed_at timestamp without time zone,
    confirmation_sent_at timestamp without time zone,
    unconfirmed_email character varying
);


ALTER TABLE users OWNER TO innovfox;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: innovfox
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO innovfox;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: innovfox
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY attachments ALTER COLUMN id SET DEFAULT nextval('attachments_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY ckeditor_assets ALTER COLUMN id SET DEFAULT nextval('ckeditor_assets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY content_blocks ALTER COLUMN id SET DEFAULT nextval('content_blocks_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY contents ALTER COLUMN id SET DEFAULT nextval('contents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY dynamic_contents ALTER COLUMN id SET DEFAULT nextval('dynamic_contents_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY friendly_id_slugs ALTER COLUMN id SET DEFAULT nextval('friendly_id_slugs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY gallery_images ALTER COLUMN id SET DEFAULT nextval('gallery_images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY gallery_types ALTER COLUMN id SET DEFAULT nextval('gallery_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY generals ALTER COLUMN id SET DEFAULT nextval('generals_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY link_types ALTER COLUMN id SET DEFAULT nextval('link_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY mercury_images ALTER COLUMN id SET DEFAULT nextval('mercury_images_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY page_link_types ALTER COLUMN id SET DEFAULT nextval('page_link_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY pages ALTER COLUMN id SET DEFAULT nextval('pages_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY taggings ALTER COLUMN id SET DEFAULT nextval('taggings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: attachments; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY attachments (id, name, image_file_name, image_content_type, image_file_size, image_updated_at, docs_file_name, docs_content_type, docs_file_size, docs_updated_at, created_at, updated_at) FROM stdin;
1	grape	food-donate-770x400-47980ad94c5bcf91f82f64dd0eb00601ef194b28dc464e63899ec9317e65f2d0.jpg	image/jpeg	58217	2016-10-02 06:03:56.857878	\N	\N	\N	\N	2016-10-02 06:03:58.027231	2016-10-02 06:03:58.027231
2	give treatment	campaign1-pic-770x400.jpg	image/jpeg	73770	2016-10-02 16:38:14.589575	\N	\N	\N	\N	2016-10-02 16:38:15.645915	2016-10-02 16:38:15.645915
3	index second container bg	earthquake-bg.jpg	image/jpeg	51389	2016-10-02 18:53:47.657177	\N	\N	\N	\N	2016-10-02 18:53:48.849526	2016-10-02 18:53:58.244965
4	events-image	event5-555x305.jpg	image/jpeg	36202	2016-10-02 20:44:14.136211	\N	\N	\N	\N	2016-10-02 20:44:14.958922	2016-10-02 20:44:14.958922
5	event2-266	event2-266x135.jpg	image/jpeg	11475	2016-10-02 21:41:52.803108	\N	\N	\N	\N	2016-10-02 21:41:53.591579	2016-10-02 21:41:53.591579
6	index_volunteer_join_block_bg	aeducation2.jpg	image/jpeg	48589	2016-10-03 06:54:23.079224	\N	\N	\N	\N	2016-10-03 06:54:24.422771	2016-10-03 06:54:24.422771
7	advisory-committee-members	testimonial-5-366x350.jpg	image/jpeg	23208	2016-10-10 06:41:54.583766	\N	\N	\N	\N	2016-10-10 06:41:55.588721	2016-10-10 06:41:55.588721
\.


--
-- Name: attachments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('attachments_id_seq', 7, true);


--
-- Data for Name: ckeditor_assets; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY ckeditor_assets (id, data_file_name, data_content_type, data_file_size, assetable_id, assetable_type, type, width, height, created_at, updated_at) FROM stdin;
\.


--
-- Name: ckeditor_assets_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('ckeditor_assets_id_seq', 1, false);


--
-- Data for Name: content_blocks; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY content_blocks (id, name, slug, created_at, updated_at) FROM stdin;
2	index-second-blocks	index-second-blocks	2016-10-02 18:52:15.426195	2016-10-02 18:52:15.426195
3	events blocks	events-blocks	2016-10-02 20:04:48.424131	2016-10-02 20:42:56.143471
4	testimonial blocks	testimonial-blocks	2016-10-03 04:49:12.687949	2016-10-03 04:52:02.081525
5	index volunteer join block	index-volunteer-join-block	2016-10-03 06:48:16.691622	2016-10-03 06:48:16.691622
6	about-left-container	about-left-container	2016-10-10 04:40:48.542279	2016-10-10 05:07:24.358132
7	about-right-container	about-right-container	2016-10-10 05:05:05.057973	2016-10-10 05:12:14.690354
8	about-bottom-container	about-bottom-container	2016-10-10 05:28:54.715347	2016-10-10 05:28:54.715347
10	advisory committee block one	advisory-committee-block-one	2016-10-10 06:33:24.536512	2016-10-10 06:33:24.536512
11	advisory-committee-block-two	advisory-committee-block-two	2016-10-10 06:50:56.857056	2016-10-10 06:50:56.857056
12	advisory-committee-block-three	advisory-committee-block-three	2016-10-10 06:54:29.56713	2016-10-10 06:54:29.56713
1	projects block	projects-block	2016-10-02 05:32:14.605724	2016-10-10 07:37:48.222986
\.


--
-- Name: content_blocks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('content_blocks_id_seq', 12, true);


--
-- Data for Name: contents; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY contents (id, content_block_id, title, sort_order, created_at, updated_at, description, brief, opt1, opt2, opt3, opt4, opt5) FROM stdin;
13	4	Arun Kumar	0	2016-10-03 05:01:27.933325	2016-10-03 05:01:27.933325	<p style="text-align: center;">Lorem ipsum dolor sit amet,</p>\r\n\r\n<p style="text-align: center;">consectet adipiscing elit.</p>\r\n\r\n<p style="text-align: center;">Nam malesuada dapi bus diam,</p>\r\n\r\n<p style="text-align: center;">ut fringilla purus efficitur eget.</p>\r\n\r\n<p style="text-align: center;">Morbi sagittis mi ac eros semper</p>\r\n\r\n<p style="text-align: center;">pharetra. Praesent sed.</p>\r\n		Code Rabbits (CTO)				
14	4	KAILASH	0	2016-10-03 05:02:10.992227	2016-10-03 05:02:10.992227	<p style="text-align: center;">Lorem ipsum dolor sit amet,</p>\r\n\r\n<p style="text-align: center;">consectet adipiscing elit.</p>\r\n\r\n<p style="text-align: center;">Nam malesuada dapi bus diam,</p>\r\n\r\n<p style="text-align: center;">ut fringilla purus efficitur eget.</p>\r\n\r\n<p style="text-align: center;">Morbi sagittis mi ac eros semper</p>\r\n\r\n<p style="text-align: center;">pharetra. Praesent sed.</p>\r\n		BIOLOTUS(ceo)				
15	5	BECOME A VOLUNTEER	0	2016-10-03 07:02:37.036911	2016-10-03 07:02:37.036911	<p>dup</p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscelit. Nam malesuada dapibus diam, ut fringilla purus efficitur eget imspurings.	FOR GOOD	JOIN US NOW		http://localhost:3000/	http://localhost:3000/system/attachments/images/000/000/006/original/aeducation2.jpg?1475477663
5	1	GIVE TREATMENT	0	2016-10-02 16:40:33.067037	2016-10-02 16:49:41.871519	<p>Phasellus in egestas libero, et congue lacus. Cras vel lacus isi.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n		FEATURED				http://localhost:3000/system/attachments/images/000/000/002/original/campaign1-pic-770x400.jpg?1475426294
6	1	GIVE education	0	2016-10-02 17:10:49.590012	2016-10-02 17:10:49.590012	<p>test content</p>\r\n						http://localhost:3000/system/attachments/images/000/000/002/original/campaign1-pic-770x400.jpg?1475426294
8	3	HOW TO BECOME A GOOD VOLUNTEER	1	2016-10-02 20:59:38.166355	2016-10-02 20:59:38.166355	<p>dup</p>\r\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam malesuada dapibus diam, ut fringilla.	UPCOMING EVENT	October 1, 2016 at 4:20 pm	North Street, 32771	http://localhost:3000/	http://localhost:3000/system/attachments/images/000/000/004/original/event5-555x305.jpg?1475441054
10	3	SEMINAR FOR CHILDREN TO LEARN ABOUT THE FUTURE	2	2016-10-02 21:44:24.883717	2016-10-02 21:48:04.981326	<p>dup</p>\r\n			November 2, 2016 at 4:20PM	Chennai, TN	http://localhost:3000/events	http://localhost:3000/system/attachments/images/000/000/004/original/event5-555x305.jpg?1475441054
11	3	SEMINAR FOR CHILDREN TO LEARN ABOUT THE FUTURE	0	2016-10-02 21:49:30.119219	2016-10-02 21:49:30.119219	<p>dup</p>\r\n			November 2, 2016 at 4:20PM	Chennai, TN	http://localhost:3000/events	http://localhost:3000/system/attachments/images/000/000/004/original/event5-555x305.jpg?1475441054
12	4	ANDY DUFRESNE	0	2016-10-03 04:51:13.356693	2016-10-03 04:51:13.356693	<p style="text-align: center;">Lorem ipsum dolor sit amet,</p>\r\n\r\n<p style="text-align: center;">consectet adipiscing elit.</p>\r\n\r\n<p style="text-align: center;">Nam malesuada dapi bus diam,</p>\r\n\r\n<p style="text-align: center;">ut fringilla purus efficitur eget.</p>\r\n\r\n<p style="text-align: center;">Morbi sagittis mi ac eros semper</p>\r\n\r\n<p style="text-align: center;">pharetra. Praesent sed.</p>\r\n		CEO MEDIA WIKI				
7	2	SPONSOR THIS PROJECT	0	2016-10-02 18:56:44.020308	2016-10-09 08:41:51.948114	<p style="text-align: center;">HELP PEOPLE TO OVERCOME</p>\r\n\r\n<p style="text-align: center;">FROM HUGE EARTHQUAKE</p>\r\n\r\n<p style="text-align: center;">Praesent diam massa, interdum quis ex id,</p>\r\n\r\n<p style="text-align: center;">laoreet interd vel ligula tortor.</p>\r\n\r\n<p style="text-align: center;">Phasellus gravida posuere orci, sed faucibus eu. Mauris fringilla.</p>\r\n\r\n<p>&nbsp;</p>\r\n		DONATE NOW	HELP NEPAL TO OVERCOME FROM HUGE EARTHQUAKE	Praesent diam massa, interdum quis ex id, laoreet interd vel ligula tortor. Phasellus gravida posuere orci, sed faucibus eu. Mauris fringilla.	http://localhost:3000/donate	http://localhost:3000/system/attachments/images/000/000/003/original/earthquake-bg.jpg?1475434427
17	7	Road Travelled	0	2016-10-10 05:17:24.272566	2016-10-10 05:18:38.481215	<p><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b id="docs-internal-guid-7d4b15f2-ad05-34f3-f5d5-1d45d5b54679" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Mithras foundation as the name denotes brightness and friendship has implemented programs to uplift the lives of the people who are in need of specialized services. </span></b></span></span></p>\r\n\r\n<p><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">The foundation after focussing on HIV/AIDS related intervention programs for over a decade has now shifted its focus towards children and women needs. The Foundation has reached to several thousand of women living with HIV .The college program of the foundation has reached more than 10000 students. The students were mainly women and they were given awareness on reproductive health and hygiene practices. Skills&rsquo; building is another area of expertise of the foundation.</span></b></span></span></p>\r\n\r\n<p><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">The training initiative for empowering the school drop outs on General duty Assistant and placing them on work was successful. The foundation was also responsible for preparing the curriculum for the content for the training. The developed curriculum and module is at present widely used for the GDA training by different players in the area of skills building.</span></b></span></span></p>\r\n		History				
16	6	WE ARE MITHRAS FOUNDATION	0	2016-10-10 04:44:37.870422	2016-10-10 05:25:37.862645	<p dir="ltr" style="line-height:1.7999999999999998;margin-top:0pt;margin-bottom:12pt;text-align: justify;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b id="docs-internal-guid-7d4b15f2-ace3-3f41-43a1-9af6447504bf" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">The foundation started its services in Tamil Nadu reaching out to hard to reach population group pregnant women who were living with HIV. The larger goal of the foundation is to take prevention and health care to women and children. </span></b></span></span></p>\r\n\r\n<p dir="ltr" style="line-height:1.7999999999999998;margin-top:0pt;margin-bottom:12pt;text-align: justify;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">The foundation has taken strides like promoting health care and awareness among women college students, Women living with HIV, Children suffering from cancer, Women and adolescents. Journey of the foundation in the last decade was focused on talking and taking health to the people who have less access to health and information. The key population group which were covered were people living with HIV, women living in urban backward settlements, people affected by Tuberculosis, children affected by cancer, college girls and student from schools etc.</span><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"> &nbsp;</span></b></span></span></p>\r\n\r\n<p dir="ltr" style="line-height:1.7999999999999998;margin-top:0pt;margin-bottom:12pt;text-align: justify;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Dr.Amudha Hari is the founder of the organization. She is a practicing gynaecologist with rich experience of handling comprehensive health issues of women. The director of the organization is Dr.R.Lakshmibai who has handled many large scale programs in various States of India. This document captures the details of activities which were implemented by Mithras Foundation during the period 2004 &ndash; 2015. In the first decade of Mithras the founder was honored with life time achiever award by government of Tamil Nadu.</span></b></span></span></p>\r\n	Mithras foundation is a Non Governmental Organization implementing programs for promoting health care in the state of Tamil Nadu since year 2004.	Who we are				
18	8	VISION	0	2016-10-10 05:34:42.29855	2016-10-10 05:45:05.151296	<p dir="ltr" style="line-height:1.7999999999999998;margin-top:0pt;margin-bottom:10pt;text-align: justify;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b id="docs-internal-guid-7d4b15f2-ad15-903a-66c1-d5edf6fe80e4" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">The vision of Mithras foundation is to provide holistic health to unreached people. The mission of the foundation is to stay committed to achieve excellence in health care, education and research.</span></b></span></span></p>\r\n\r\n<ul dir="ltr">\r\n\t<li style="list-style-type: disc; font-size: 18.6667px; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b id="docs-internal-guid-7d4b15f2-ad15-903a-66c1-d5edf6fe80e4" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">The main objectives of the foundation are</span></b></span></span></li>\r\n\t<li style="list-style-type: disc; font-size: 18.6667px; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Working with vulnerable and marginalized children.</span></b></span></span></li>\r\n\t<li style="list-style-type: disc; font-size: 18.6667px; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b id="docs-internal-guid-7d4b15f2-ad15-903a-66c1-d5edf6fe80e4" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Addressing the health needs of women in the reproductive age group.</span></b></span></span></span></b></span></span></li>\r\n\t<li style="list-style-type: disc; font-size: 18.6667px; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b id="docs-internal-guid-7d4b15f2-ad15-903a-66c1-d5edf6fe80e4" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Capacity building of health care providers with emphasis on universal precautions.</span></b></span></span></span></b></span></span></span></b></span></span></li>\r\n\t<li style="list-style-type: disc; font-size: 18.6667px; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b id="docs-internal-guid-7d4b15f2-ad15-903a-66c1-d5edf6fe80e4" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Reduction of stigma and discrimination against People Living with HIV (PLHIV).</span></b></span></span></span></b></span></span></span></b></span></span></span></b></span></span></li>\r\n\t<li style="list-style-type: disc; font-size: 18.6667px; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b id="docs-internal-guid-7d4b15f2-ad15-903a-66c1-d5edf6fe80e4" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Reaching out to youth through colleges and schools to address the low-risk perceptions among young people.</span></b></span></span></span></b></span></span></span></b></span></span></span></b></span></span></span></b></span></span></li>\r\n\t<li style="list-style-type: disc; font-size: 18.6667px; font-family: Arial; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt; text-align: justify;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="font-size:14px;"><span style="font-family: Comic Sans MS,cursive;"><b id="docs-internal-guid-7d4b15f2-ad15-903a-66c1-d5edf6fe80e4" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Dispelling myths and misconceptions among general populations about reproductive health.</span></b></span></span></span></b></span></span></span></b></span></span></span></b></span></span></span></b></span></span></span></b></span></span></li>\r\n</ul>\r\n		VISION				
19	10	Dr. Amudha Hari	0	2016-10-10 06:42:33.458416	2016-10-10 06:42:33.458416	<p>dup</p>\r\n		Founder			http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714	http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714
20	10	Mrs. Asma	0	2016-10-10 06:43:31.681264	2016-10-10 06:43:31.681264	<p>dup</p>\r\n		staff volunteer			http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714	http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714
21	10	Mrs. Padma	0	2016-10-10 06:44:41.037656	2016-10-10 06:44:41.037656	<p>dup</p>\r\n		staff volunteer			http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714	http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714
22	11	Justice Vallinayagam	0	2016-10-10 06:52:17.19196	2016-10-10 06:52:17.19196	<p>dup</p>\r\n		Lok Adalat, Madras High Court			http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714	http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714
23	11	Dr Charumathi Natrajan	0	2016-10-10 06:53:07.747795	2016-10-10 06:53:07.747795	<p>dup</p>\r\n		Senior Gynecologist			http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714	http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714
24	11	Dr Kuganantham	0	2016-10-10 06:53:46.367439	2016-10-10 06:53:46.367439	<p>dup</p>\r\n		City Health Officer			http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714	http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714
25	12	Arun Kumar K	0	2016-10-10 06:55:21.085586	2016-10-10 06:55:21.085586	<p>dup</p>\r\n		CTO - Code Rabbits Technologies			http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714	http://localhost:3000/system/attachments/images/000/000/007/original/testimonial-5-366x350.jpg?1476081714
\.


--
-- Name: contents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('contents_id_seq', 25, true);


--
-- Data for Name: dynamic_contents; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY dynamic_contents (id, title, slug, created_at, updated_at, description) FROM stdin;
3	third slider	third-slider	2016-09-30 11:11:58.837356	2016-09-30 11:14:18.033007	<h2 style="font-style: italic; text-align: center;"><span style="color:#FFFFFF;"><span style="font-size: 26px;">first sliderfirst sliderfirst slider</span></span></h2>\r\n\r\n<h2 style="font-style: italic; text-align: center;"><span style="color:#FFFFFF;"><span style="font-size: 26px;">first sliderfirst slider</span></span></h2>\r\n\r\n<p style="text-align: center;"><span style="color:#FFFFFF;"><span style="font-size: 26px;"><span style="background-color:#000000;">View More</span></span></span></p>\r\n
4	first slider	first-slider	2016-09-30 11:42:47.998417	2016-09-30 11:42:47.998417	<p style="text-align: center;"><span style="color:#FFFFFF;"><strong><span style="font-size: 26px;">We Help Thousand of Child to </span></strong></span></p>\r\n\r\n<p style="text-align: center;"><span style="color:#FFFFFF;"><strong><span style="font-size: 26px;">Get Well</span></strong></span></p>\r\n
2	second slider	second-slider	2016-09-30 10:31:12.210218	2016-09-30 11:09:14.548557	<p><span class="sd-featured-label">Pink House</span> <span class="sd-days-left ">contribute</span></p>\r\n\r\n<div class="clearfix">&nbsp;</div>\r\n\r\n<h3><a href="events.html" title="VACCINE CARRIER FOR AFRICAN CHILDREN">VACCINE CARRIER FOR AFRICAN CHILDREN</a></h3>\r\n\r\n<div class="sd-campaign-excerpt">\r\n<p>Phasellus in egestas libero, et congue lacus. Cras vel lacus nisi. Duis nulla metus, tincidunt at tortor.</p>\r\n</div>\r\n\r\n<div class="clearfix">&nbsp;</div>\r\n\r\n<div class="sd-donate-button-wrapper"><a 42="" class="bb-link" href="donate.html" style="background:#29af8a;color:#ffffff; data-campaign-id=">View More</a></div>\r\n<!-- sd-donate-button-wrapper -->
5	title-slider-box-one	title-slider-box-one	2016-09-30 11:45:28.402565	2016-09-30 11:49:11.161893	<p><span style="color:#FFFFFF;"><span style="font-size: 16px;"><span style="font-family: arial,helvetica,sans-serif;"><strong>GIVE DONATION</strong></span></span></span></p>\r\n
6	dis-slider-box-one	dis-slider-box-one	2016-09-30 11:50:16.789875	2016-09-30 11:52:09.751681	<p><span style="color:#D3D3D3;">Lorem ipsum dolor sit, cons purus efficitur eget.</span></p>\r\n
7	title-slider-box-two	title-slider-box-two	2016-09-30 11:53:32.359865	2016-09-30 11:53:32.359865	<p><span style="color:#FFFFFF;"><strong><span style="font-family: arial,helvetica,sans-serif;"><span style="font-size: 16px;">BECOME A VOLUNTEER</span></span></strong></span></p>\r\n
8	des-slider-box-two 	des-slider-box-two	2016-09-30 11:54:58.842186	2016-09-30 11:54:58.842186	<p><span style="color:#D3D3D3;">Lorem ipsum dolor sit, cons purus efficitur eget.</span></p>\r\n
9	title-slider-box-third 	title-slider-box-third	2016-09-30 11:56:55.401266	2016-09-30 11:56:55.401266	<p><span style="color:#FFFFFF;"><strong><span style="font-size: 16px;"><span style="font-family: arial,helvetica,sans-serif;">GIVE SCHOLARSHIP</span></span></strong></span></p>\r\n
10	des-slider-box-third 	des-slider-box-third	2016-09-30 11:58:12.963078	2016-09-30 11:58:12.963078	<p><span style="color:#D3D3D3;">Lorem ipsum dolor sit, cons purus efficitur eget.</span></p>\r\n
11	first-container-heading	first-container-heading	2016-10-02 04:49:14.714411	2016-10-02 05:24:57.441236	PINK HOUSE
12	first-container-des	first-container-des	2016-10-02 05:28:29.674693	2016-10-02 05:29:43.928359	<p style="text-align: center;"><span style="font-size:18px;"><span style="color: rgb(105, 105, 105);"><strong>WE HELP THOUSANDS OF CHILDREN TO GET WELL</strong></span></span></p>\r\n\r\n<p style="text-align: center;"><span style="font-size:18px;"><span style="color: rgb(105, 105, 105);"><strong>NOW WE NEED YOUR SUPPORT</strong></span></span></p>\r\n
1	about-footer	about-footer	2016-09-26 11:45:48.547785	2016-10-02 13:26:45.989556	<p>Test Praesent diam massa, interdum qu vel ligula tortor. Phasellus gravida faucibus eu. Mauris fringilla place et fermentum hendrerit.</p>\r\n
14	index testimonial description	index-testimonial-description	2016-10-03 04:38:16.928275	2016-10-03 04:40:17.668859	<p>WHAT OUR DONORS OVER THE WORLD ARE SAYING test</p>\r\n
15	advisory-committee-title-one	advisory-committee-title-one	2016-10-10 06:11:13.419181	2016-10-10 06:11:13.419181	OUR CORE TEAM\r\n
20	advisory-committee-des-three	advisory-committee-des-three	2016-10-10 07:15:17.112486	2016-10-10 07:15:17.112486	<p><span style="font-size:16px;"><span style="font-family: Georgia,serif;"><span style="color: rgb(145, 161, 180); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke: 0.001px transparent; background-color: rgb(255, 255, 255); display: inline ! important; float: none;">We support a talented team of professionals to accomplish Children Fund&rsquo;s goals and mission.</span></span></span></p>\r\n
16	advisory-committee-des-one	advisory-committee-des-one	2016-10-10 06:16:02.805592	2016-10-10 06:32:43.0045	<p><span style="font-size:16px;"><span style="font-family: Georgia,serif;"><span style="color: rgb(145, 161, 180); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke: 0.001px transparent; background-color: rgb(255, 255, 255); display: inline ! important; float: none;">Founder Dr. Amudha Hari and others work with a talented team of professionals to accomplish Children Fund&rsquo;s goals and mission.</span></span></span></p>\r\n
17	advisory-committee-title-two	advisory-committee-title-two	2016-10-10 07:07:38.165386	2016-10-10 07:07:38.165386	TECHNICAL ADVISORY GROUP\r\n
18	advisory-committee-title-three	advisory-committee-title-three	2016-10-10 07:08:21.070766	2016-10-10 07:08:21.070766	<p>VOLUNTEERS</p>\r\n
19	advisory-committee-des-two	advisory-committee-des-two	2016-10-10 07:14:15.87601	2016-10-10 07:14:15.87601	<p><span style="font-size:16px;"><span style="font-family: Georgia,serif;"><span style="color: rgb(145, 161, 180); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; text-align: left; text-indent: 0px; text-transform: none; white-space: normal; word-spacing: 0px; -webkit-text-stroke: 0.001px transparent; background-color: rgb(255, 255, 255); display: inline ! important; float: none;">We are technically support talented team of professionals to accomplish Children Fund&rsquo;s goals and mission.</span></span></span></p>\r\n
21	projects-tag	projects-tag	2016-10-10 08:27:49.563082	2016-10-10 08:27:49.563082	HOW CAN YOU HELP\r\n
22	projects-description	projects-description	2016-10-10 08:29:37.734737	2016-10-10 08:29:37.734737	WE NEED YOUR HELP TO HELP OTHERS, SEE PROJECTS
24	contact-title	contact-title	2016-10-10 13:08:16.896144	2016-10-10 13:08:16.896144	<h3 style="font-family:'Montserrat';font-weight:700;margin-bottom:15px;">HAVE AN IDEA TO HELP? - CONTACT US</h3>\r\n
25	contact-description	contact-description	2016-10-10 13:09:59.367316	2016-10-10 13:09:59.367316	<p><span style="font-size:14px;"><span style="color: rgb(0, 100, 0);">Pellentesque mollis eros quis massa interdum porta et vel nisi. Duis vel viverra quamam molesvulputate femy contenteu.</span></span></p>\r\n
23	pink-house	pink-house	2016-10-10 12:40:47.288981	2016-10-10 12:49:03.192567	<p dir="ltr" style="line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;"><span style="font-size:14px;"><b id="docs-internal-guid-7d4b15f2-ae92-7f9c-44c4-2ba35aaf7992" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 700; font-style: normal; font-variant: normal; text-decoration: underline; vertical-align: baseline; white-space: pre-wrap;">Psycho Social and Nutritional support program for children with cancer &amp; their parents</span></b></span></p>\r\n\r\n<p dir="ltr" style="line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;">&nbsp;</p>\r\n\r\n<p dir="ltr" style="line-height:1.3800000000000001;margin-top:0pt;margin-bottom:10pt;"><span style="font-size:14px;"><b id="docs-internal-guid-7d4b15f2-ae93-3b97-0aea-b0d10d74ec4e" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Foundation has taken pioneer effort to implement supportive program to children affected by cancer. &nbsp;Children supported are from low economic status and were in the age group of 0 to 18 years. </span><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"><span style="white-space: pre;"> </span></span><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Initiative &nbsp;provides financial support for their</span></b></span></p>\r\n\r\n<ul>\r\n\t<li><span style="font-size:14px;"><b id="docs-internal-guid-7d4b15f2-ae94-1ca6-a6a5-687d69e9adc1" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Treatment cost which they incur in the networked hospitals </span></b></span></li>\r\n\t<li><span style="font-size:14px;"><b id="docs-internal-guid-7d4b15f2-ae94-3117-e1ac-f9faca57c94b" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Counseling to parents on coping with stress </span></b></span></li>\r\n\t<li><span style="font-size:14px;"><b id="docs-internal-guid-7d4b15f2-ae94-44dc-e74a-a7205241cc7f" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hygiene and treatment adherence </span></b></span></li>\r\n\t<li><span style="font-size:14px;"><b id="docs-internal-guid-7d4b15f2-ae94-58a9-367d-eb7d53e37ae5" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: italic; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nutrition and educational support grants</span><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;"> </span></b></span></li>\r\n</ul>\r\n\r\n<p dir="ltr" style="line-height:1.7999999999999998;margin-top:0pt;margin-bottom:10pt;text-align: justify;"><span style="font-size:14px;"><b id="docs-internal-guid-7d4b15f2-ae95-1c6c-4a61-f341e6cb209a" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">This initiative is functional since year 2015; Mithras is planning to scale up this initiative . The foundation has studied the major reasons for children dropping out from treatment.The salient reasons are listed below:</span></b></span></p>\r\n\r\n<p><span style="font-size:14px;"><b id="docs-internal-guid-7d4b15f2-ae95-1c6c-4a61-f341e6cb209a" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Mithras is in the process of establishing a transit home which is branded as &ldquo;Pink house&rdquo; .The transit home provides short stay and food requirements of the children and the parents. In addition the transit home has set of activities which help the parents and children cope up with the stress and get encouraged to respond positively for the treatment.</span></b></span></p>\r\n\r\n<ol>\r\n\t<li><span style="font-family:Comic Sans MS,cursive;"><span style="font-size: 14px;"><b id="docs-internal-guid-7d4b15f2-aea1-2603-3dda-3bbf8c4d7102" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Children come from other states or from remote locations of TN, AP&amp;Bihar </span></b></span></span></li>\r\n\t<li><span style="font-family:Comic Sans MS,cursive;"><span style="font-size: 14px;"><b id="docs-internal-guid-7d4b15f2-aea1-424d-bacc-8bd62091adbf" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Parents unaware about the importance of treatment for cancer </span></b></span></span></li>\r\n\t<li><span style="font-family:Comic Sans MS,cursive;"><span style="font-size: 14px;"><b id="docs-internal-guid-7d4b15f2-aea1-a0b1-2db4-66d0b1a406df" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Less risk perception by the parents </span></b></span></span></li>\r\n\t<li><span style="font-family:Comic Sans MS,cursive;"><span style="font-size: 14px;"><b id="docs-internal-guid-7d4b15f2-aea1-beff-a554-9daecb3085ee" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Cost of the treatment and stay </span></b></span></span></li>\r\n\t<li><span style="font-family:Comic Sans MS,cursive;"><span style="font-size: 14px;"><b id="docs-internal-guid-7d4b15f2-aea1-d78b-e583-053a20367e38" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Less knowledge of parents to manage the sudden stress after the child diagnosed with cancer</span></b></span></span></li>\r\n\t<li><span style="font-family:Comic Sans MS,cursive;"><span style="font-size: 14px;"><b id="docs-internal-guid-7d4b15f2-aea1-ee77-3af9-868ec8089055" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Denial of the diagnosis </span></b></span></span></li>\r\n\t<li><span style="font-family:Comic Sans MS,cursive;"><span style="font-size: 14px;"><b id="docs-internal-guid-7d4b15f2-aea2-0943-93a7-2ff29c9a15fe" style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Forced to stay in unhygienic stay places and subject themselves to high possibilities of developing other complication during treatment phase etc </span></b></span></span></li>\r\n</ol>\r\n\r\n<p><span style="font-size:14px;"><b style="font-weight:normal;"><span style="color: rgb(0, 0, 0); background-color: transparent; font-weight: 700; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">GET INVOLVED</span></b></span></p>\r\n\r\n<p dir="ltr" style="line-height:1.7999999999999998;margin-top:0pt;margin-bottom:10pt;margin-left: 36pt;"><span style="font-size:14px;"><b id="docs-internal-guid-7d4b15f2-aea0-8177-3521-5a04df8ca52b" style="font-weight:normal;"><span style="font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">Mithras Foundation believes that if others join hands for various social causes it will be of great support to lift the people from their miseries. Individuals and organisations are invited to collaborate for the various causes for which Mithras is serving. </span></b></span></p>\r\n\r\n<p><span style="font-size:14px;"><b id="docs-internal-guid-7d4b15f2-aea0-8177-3521-5a04df8ca52b" style="font-weight:normal;"><span style="font-family: &quot;Times New Roman&quot;; color: rgb(0, 0, 0); background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre-wrap;">The first priority of service will be to support children diagnosed with cancer. You can support Mithras Foundation to support the individual child or you can directly support the child. In this page we will keep updating the individual child needs. Your gesture to support goes a long way to save the young life. The details of the children and their parents contact number is provided below for your support. We request you to kindly keep us informed if you prefer to support the child directly. This measure is taken to avoid duplication and other essential care.</span></b></span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n
\.


--
-- Name: dynamic_contents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('dynamic_contents_id_seq', 25, true);


--
-- Data for Name: friendly_id_slugs; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY friendly_id_slugs (id, slug, sluggable_id, sluggable_type, scope, created_at) FROM stdin;
1	test	1	Page	\N	2016-09-18 08:28:21.203416
\.


--
-- Name: friendly_id_slugs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('friendly_id_slugs_id_seq', 1, true);


--
-- Data for Name: gallery_images; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY gallery_images (id, avatar_file_name, avatar_content_type, avatar_file_size, avatar_updated_at, gallery_type_id, alt_text, sort_order, link, description, title, created_at, updated_at) FROM stdin;
4	home1-slide3-100x50.jpg	image/jpeg	3406	2016-09-30 07:47:03.257937	2		1	http://		first image	2016-09-30 07:47:04.451889	2016-09-30 07:47:04.451889
5	home1-slide2-100x50.jpg	image/jpeg	4076	2016-09-30 07:47:43.944158	2		2	http://		second image	2016-09-30 07:47:44.239777	2016-09-30 07:47:44.239777
6	slide-2-kids-no-shadow-100x50.jpg	image/jpeg	3310	2016-09-30 07:48:16.371068	2		3	http://		third image	2016-09-30 07:48:16.604105	2016-09-30 07:48:16.604105
7	home1-slide3.jpg	image/jpeg	142984	2016-09-30 09:57:04.828538	3		4	http://		first image	2016-09-30 09:57:05.861718	2016-09-30 09:57:05.861718
8	home1-slide2.jpg	image/jpeg	167175	2016-09-30 10:19:33.089244	3		5	http://		second image	2016-09-30 10:19:34.3094	2016-09-30 10:19:34.3094
9	slide-2-kids-no-shadow.jpg	image/jpeg	153094	2016-09-30 10:20:08.84167	3		6	http://		third image	2016-09-30 10:20:09.39299	2016-09-30 10:20:09.39299
10	campaign-title-bg.jpg	image/jpeg	88621	2016-10-10 03:44:11.897517	4	mithras-header	7	http://		mithras header image	2016-10-10 03:44:13.260956	2016-10-10 03:44:13.260956
\.


--
-- Name: gallery_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('gallery_images_id_seq', 10, true);


--
-- Data for Name: gallery_types; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY gallery_types (id, name, height, width, is_video, image_type, slug, created_at, updated_at) FROM stdin;
2	index-slider-small-images	50	100	f	index-slider-small-images	\N	2016-09-30 07:44:13.358467	2016-09-30 09:38:45.02341
3	index-slider-images	750	1970	f	index-slider-images	\N	2016-09-30 09:56:21.656854	2016-09-30 09:58:06.153316
\.


--
-- Name: gallery_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('gallery_types_id_seq', 4, true);


--
-- Data for Name: generals; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY generals (id, logo_file_name, logo_content_type, logo_file_size, logo_updated_at, favicon_file_name, favicon_content_type, favicon_file_size, favicon_updated_at, page_title, meta_description, analytics_text, created_at, updated_at, phone_number, fb_link, twitter_link, address, email_id) FROM stdin;
1	mf-logo.png	image/png	4134	2016-09-20 11:44:21.742396	\N	\N	\N	\N	\N	\N		2016-09-16 10:51:33.169604	2016-10-10 13:23:16.641095	+(91) 9444475750	http://www.coderabbits.com/	http://www.coderabbits.com/	<p>Mithras Foundation<br />\r\nR-7,Corporation Main Road,<br />\r\nPreungudi, CHENNAI<br />\r\nTamilnadu</p>\r\n	admin@mithrasfoundation.org
\.


--
-- Name: generals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('generals_id_seq', 1, true);


--
-- Data for Name: link_types; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY link_types (id, name, created_at, updated_at) FROM stdin;
1	header_link	2016-09-18 09:17:57.335903	2016-09-18 09:17:57.335903
\.


--
-- Name: link_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('link_types_id_seq', 1, true);


--
-- Data for Name: mercury_images; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY mercury_images (id, image_file_name, image_content_type, image_file_size, image_updated_at, created_at, updated_at) FROM stdin;
\.


--
-- Name: mercury_images_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('mercury_images_id_seq', 1, false);


--
-- Data for Name: page_link_types; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY page_link_types (id, page_id, link_type_id, created_at, updated_at, parent_id) FROM stdin;
1	1	1	2016-09-18 09:18:04.059992	2016-09-18 09:18:04.059992	\N
\.


--
-- Name: page_link_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('page_link_types_id_seq', 1, true);


--
-- Data for Name: pages; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY pages (id, title, parent_id, meta_title, meta_description, slug, status, sort_order, created_at, updated_at, description) FROM stdin;
1	test	\N	test	test	test	t	0	2016-09-18 08:28:21.162225	2016-09-18 08:28:21.162225	<p>test</p>\r\n
\.


--
-- Name: pages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('pages_id_seq', 1, true);


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY schema_migrations (version) FROM stdin;
20160209153701
20160209153702
20160209153703
20160209153704
20160209153705
20160209153706
20160209153707
20160209153708
20160209153709
20160210081013
20160215050257
20160215101358
20160215101638
20160215120212
20160215121727
20160216053336
20160218133041
20160220061516
20160223100110
20160226063009
20161002125954
20161002144828
20161002152227
\.


--
-- Data for Name: taggings; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY taggings (id, tag_id, gallery_image_id, created_at, updated_at) FROM stdin;
\.


--
-- Name: taggings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('taggings_id_seq', 3, true);


--
-- Data for Name: tags; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY tags (id, name, created_at, updated_at) FROM stdin;
1	mini-slider-image-1	2016-09-20 12:40:37.619382	2016-09-20 12:40:37.619382
\.


--
-- Name: tags_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('tags_id_seq', 1, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: innovfox
--

COPY users (id, email, encrypted_password, reset_password_token, reset_password_sent_at, remember_created_at, sign_in_count, current_sign_in_at, last_sign_in_at, current_sign_in_ip, last_sign_in_ip, created_at, updated_at, confirmation_token, confirmed_at, confirmation_sent_at, unconfirmed_email) FROM stdin;
1	arun@coderabbits.com	$2a$10$Ym3eVW7TjgnSJhVsAxtjH.PO9o1Bu9qX.v/BzS267WCRj.VYXCkmC	\N	\N	\N	12	2016-10-10 08:26:51.908023	2016-10-09 08:39:18.898329	127.0.0.1	127.0.0.1	2016-09-18 08:05:29.774279	2016-10-10 08:26:51.910111	w5q6HecR8S1w9sGAQ6jB	2016-09-18 08:06:37.267561	2016-09-18 08:05:30.029535	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: innovfox
--

SELECT pg_catalog.setval('users_id_seq', 1, true);


--
-- Name: attachments_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY attachments
    ADD CONSTRAINT attachments_pkey PRIMARY KEY (id);


--
-- Name: ckeditor_assets_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY ckeditor_assets
    ADD CONSTRAINT ckeditor_assets_pkey PRIMARY KEY (id);


--
-- Name: content_blocks_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY content_blocks
    ADD CONSTRAINT content_blocks_pkey PRIMARY KEY (id);


--
-- Name: contents_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY contents
    ADD CONSTRAINT contents_pkey PRIMARY KEY (id);


--
-- Name: dynamic_contents_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY dynamic_contents
    ADD CONSTRAINT dynamic_contents_pkey PRIMARY KEY (id);


--
-- Name: friendly_id_slugs_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY friendly_id_slugs
    ADD CONSTRAINT friendly_id_slugs_pkey PRIMARY KEY (id);


--
-- Name: gallery_images_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY gallery_images
    ADD CONSTRAINT gallery_images_pkey PRIMARY KEY (id);


--
-- Name: gallery_types_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY gallery_types
    ADD CONSTRAINT gallery_types_pkey PRIMARY KEY (id);


--
-- Name: generals_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY generals
    ADD CONSTRAINT generals_pkey PRIMARY KEY (id);


--
-- Name: link_types_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY link_types
    ADD CONSTRAINT link_types_pkey PRIMARY KEY (id);


--
-- Name: mercury_images_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY mercury_images
    ADD CONSTRAINT mercury_images_pkey PRIMARY KEY (id);


--
-- Name: page_link_types_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY page_link_types
    ADD CONSTRAINT page_link_types_pkey PRIMARY KEY (id);


--
-- Name: pages_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY pages
    ADD CONSTRAINT pages_pkey PRIMARY KEY (id);


--
-- Name: taggings_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY taggings
    ADD CONSTRAINT taggings_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: idx_ckeditor_assetable; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE INDEX idx_ckeditor_assetable ON ckeditor_assets USING btree (assetable_type, assetable_id);


--
-- Name: idx_ckeditor_assetable_type; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE INDEX idx_ckeditor_assetable_type ON ckeditor_assets USING btree (assetable_type, type, assetable_id);


--
-- Name: index_friendly_id_slugs_on_slug_and_sluggable_type; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE INDEX index_friendly_id_slugs_on_slug_and_sluggable_type ON friendly_id_slugs USING btree (slug, sluggable_type);


--
-- Name: index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE UNIQUE INDEX index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope ON friendly_id_slugs USING btree (slug, sluggable_type, scope);


--
-- Name: index_friendly_id_slugs_on_sluggable_id; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE INDEX index_friendly_id_slugs_on_sluggable_id ON friendly_id_slugs USING btree (sluggable_id);


--
-- Name: index_friendly_id_slugs_on_sluggable_type; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE INDEX index_friendly_id_slugs_on_sluggable_type ON friendly_id_slugs USING btree (sluggable_type);


--
-- Name: index_taggings_on_gallery_image_id; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE INDEX index_taggings_on_gallery_image_id ON taggings USING btree (gallery_image_id);


--
-- Name: index_taggings_on_tag_id; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE INDEX index_taggings_on_tag_id ON taggings USING btree (tag_id);


--
-- Name: index_users_on_confirmation_token; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE UNIQUE INDEX index_users_on_confirmation_token ON users USING btree (confirmation_token);


--
-- Name: index_users_on_email; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE UNIQUE INDEX index_users_on_email ON users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: innovfox
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- Name: fk_rails_9fcd2e236b; Type: FK CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY taggings
    ADD CONSTRAINT fk_rails_9fcd2e236b FOREIGN KEY (tag_id) REFERENCES tags(id);


--
-- Name: fk_rails_ef635c425c; Type: FK CONSTRAINT; Schema: public; Owner: innovfox
--

ALTER TABLE ONLY taggings
    ADD CONSTRAINT fk_rails_ef635c425c FOREIGN KEY (gallery_image_id) REFERENCES gallery_images(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

