# == Schema Information
#
# Table name: pages
#
#  id               :integer          not null, primary key
#  title            :string
#  parent_id        :integer
#  meta_title       :string
#  meta_description :string
#  slug             :string
#  status           :boolean
#  sort_order       :integer          default(0)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  description      :text
#

class Page < ActiveRecord::Base
    include TheSortableTree::Scopes
	extend FriendlyId
    extend ActsAsTree::TreeView
    extend ActsAsTree::TreeWalker
    friendly_id :title, use: [:slugged, :history]
    acts_as_tree order: 'title'
    has_many :page_link_types, class_name: "PageLinkType"
    has_many :link_types, through: :page_link_types, class_name: "LinkType"
    validates :title, presence: true
    validates :description, presence: true

    def should_generate_new_friendly_id?
      title_changed?
    end
end