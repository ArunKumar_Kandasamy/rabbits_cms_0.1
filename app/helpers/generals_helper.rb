# == Schema Information
#
# Table name: generals
#
#  id                   :integer          not null, primary key
#  logo_file_name       :string
#  logo_content_type    :string
#  logo_file_size       :integer
#  logo_updated_at      :datetime
#  favicon_file_name    :string
#  favicon_content_type :string
#  favicon_file_size    :integer
#  favicon_updated_at   :datetime
#  page_title           :string
#  meta_description     :text
#  analytics_text       :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  phone_number         :string
#  fb_link              :string
#  twitter_link         :string
#  address              :text
#  email_id             :string
#

module GeneralsHelper
end
