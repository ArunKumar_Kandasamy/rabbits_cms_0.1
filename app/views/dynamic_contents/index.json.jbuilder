json.array!(@dynamic_contents) do |dynamic_content|
  json.extract! dynamic_content, :id, :title, :description, :slug
  json.url dynamic_content_url(dynamic_content, format: :json)
end
