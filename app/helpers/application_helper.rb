module ApplicationHelper

	def general
		@general = General.first
	end

	def dynamic_content(slug)
		@dynamic_content = DynamicContent.find_by_slug(slug)
		if @dynamic_content.present?
		   return @dynamic_content.description
		end
	end

	def content_block(slug)
		@content_block = ContentBlock.find_by_slug(slug)
	end

	def gallery_type(slug)
		@gallery_type = GalleryType.find_by_image_type(slug)
	end

	def link_type(name)
		@link_type = LinkType.find_by_name(name)
	end

end
