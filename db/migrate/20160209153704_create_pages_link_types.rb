class CreatePagesLinkTypes < ActiveRecord::Migration
  def change
    create_table :pages_link_types do |t|
      t.integer :page_id
      t.integer :link_type_id

      t.timestamps null: false
    end
  end
end
