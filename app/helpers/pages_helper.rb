# == Schema Information
#
# Table name: pages
#
#  id               :integer          not null, primary key
#  title            :string
#  parent_id        :integer
#  meta_title       :string
#  meta_description :string
#  slug             :string
#  status           :boolean
#  sort_order       :integer          default(0)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  description      :text
#

module PagesHelper
end
