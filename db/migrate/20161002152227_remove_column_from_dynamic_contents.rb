class RemoveColumnFromDynamicContents < ActiveRecord::Migration
  def change
    remove_column :dynamic_contents, :brief, :text
    remove_column :dynamic_contents, :opt1, :string
    remove_column :dynamic_contents, :opt2, :string
    remove_column :dynamic_contents, :opt3, :string
    remove_column :dynamic_contents, :opt4, :string
    remove_column :dynamic_contents, :opt5, :string
  end
end
