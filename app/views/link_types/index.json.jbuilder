json.array!(@link_types) do |link_type|
  json.extract! link_type, :id, :name
  json.url link_type_url(link_type, format: :json)
end
