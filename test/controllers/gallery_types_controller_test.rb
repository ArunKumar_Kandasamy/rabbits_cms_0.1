# == Schema Information
#
# Table name: gallery_types
#
#  id         :integer          not null, primary key
#  name       :string
#  height     :integer
#  width      :integer
#  is_video   :boolean          default(FALSE)
#  image_type :string
#  slug       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

require 'test_helper'

class GalleryTypesControllerTest < ActionController::TestCase
  setup do
    @gallery_type = gallery_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gallery_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gallery_type" do
    assert_difference('GalleryType.count') do
      post :create, gallery_type: { height: @gallery_type.height, image_type: @gallery_type.image_type, is_video: @gallery_type.is_video, name: @gallery_type.name, slug: @gallery_type.slug, width: @gallery_type.width }
    end

    assert_redirected_to gallery_type_path(assigns(:gallery_type))
  end

  test "should show gallery_type" do
    get :show, id: @gallery_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gallery_type
    assert_response :success
  end

  test "should update gallery_type" do
    patch :update, id: @gallery_type, gallery_type: { height: @gallery_type.height, image_type: @gallery_type.image_type, is_video: @gallery_type.is_video, name: @gallery_type.name, slug: @gallery_type.slug, width: @gallery_type.width }
    assert_redirected_to gallery_type_path(assigns(:gallery_type))
  end

  test "should destroy gallery_type" do
    assert_difference('GalleryType.count', -1) do
      delete :destroy, id: @gallery_type
    end

    assert_redirected_to gallery_types_path
  end
end
