json.array!(@pages) do |page|
  json.extract! page, :id, :title, :description, :parent_id, :meta_title, :meta_description, :slug, :status, :sort_order
  json.url page_url(page, format: :json)
end
