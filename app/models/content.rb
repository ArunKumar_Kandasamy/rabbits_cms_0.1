# == Schema Information
#
# Table name: contents
#
#  id               :integer          not null, primary key
#  content_block_id :integer
#  title            :string
#  sort_order       :integer          default(0)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  description      :text
#  brief            :text
#  opt1             :string
#  opt2             :string
#  opt3             :string
#  opt4             :string
#  opt5             :string
#

class Content < ActiveRecord::Base
	has_many :content_block
    validates :title, presence: true
    validates :description, presence: true
end
