json.array!(@gallery_images) do |gallery_image|
  json.extract! gallery_image, :id, :avatar, :gallery_type_id, :alt_text, :sort_order, :link, :description, :title
  json.url gallery_image_url(gallery_image, format: :json)
end
