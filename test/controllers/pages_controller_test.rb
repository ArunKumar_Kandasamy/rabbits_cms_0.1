# == Schema Information
#
# Table name: pages
#
#  id               :integer          not null, primary key
#  title            :string
#  parent_id        :integer
#  meta_title       :string
#  meta_description :string
#  slug             :string
#  status           :boolean
#  sort_order       :integer          default(0)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  description      :text
#

require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  setup do
    @page = pages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create page" do
    assert_difference('Page.count') do
      post :create, page: { description: @page.description, meta_description: @page.meta_description, meta_title: @page.meta_title, parent_id: @page.parent_id, slug: @page.slug, sort_order: @page.sort_order, status: @page.status, title: @page.title }
    end

    assert_redirected_to page_path(assigns(:page))
  end

  test "should show page" do
    get :show, id: @page
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @page
    assert_response :success
  end

  test "should update page" do
    patch :update, id: @page, page: { description: @page.description, meta_description: @page.meta_description, meta_title: @page.meta_title, parent_id: @page.parent_id, slug: @page.slug, sort_order: @page.sort_order, status: @page.status, title: @page.title }
    assert_redirected_to page_path(assigns(:page))
  end

  test "should destroy page" do
    assert_difference('Page.count', -1) do
      delete :destroy, id: @page
    end

    assert_redirected_to pages_path
  end
end
