require 'test_helper'

class PagesLinkTypesControllerTest < ActionController::TestCase
  setup do
    @pages_link_type = pages_link_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:pages_link_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create pages_link_type" do
    assert_difference('PagesLinkType.count') do
      post :create, pages_link_type: { link_type: @pages_link_type.link_type, page_id: @pages_link_type.page_id }
    end

    assert_redirected_to pages_link_type_path(assigns(:pages_link_type))
  end

  test "should show pages_link_type" do
    get :show, id: @pages_link_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @pages_link_type
    assert_response :success
  end

  test "should update pages_link_type" do
    patch :update, id: @pages_link_type, pages_link_type: { link_type: @pages_link_type.link_type, page_id: @pages_link_type.page_id }
    assert_redirected_to pages_link_type_path(assigns(:pages_link_type))
  end

  test "should destroy pages_link_type" do
    assert_difference('PagesLinkType.count', -1) do
      delete :destroy, id: @pages_link_type
    end

    assert_redirected_to pages_link_types_path
  end
end
