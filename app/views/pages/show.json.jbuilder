json.extract! @page, :id, :title, :description, :parent_id, :meta_title, :meta_description, :slug, :status, :sort_order, :created_at, :updated_at
